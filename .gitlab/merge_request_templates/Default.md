Add a description of the new feature/bug fix. Reference any relevant bugs.

## Checklist
 * [ ] Code modified for feature
 * [ ] Documentation updated / CHANGELOG.md entry present (for non-trivial changes)

## Reviewer's checklist:
 * [ ] Any issues marked for closing are addressed
 * [ ] Function naming, parameters, return values, types, etc., are consistent and according to [CONTRIBUTING.md](https://gitlab.com/openconnect/openconnect-gui/-/raw/master/CONTRIBUTING.md)
 * [ ] This feature/change has adequate documentation added
 * [ ] No obvious mistakes in the code

